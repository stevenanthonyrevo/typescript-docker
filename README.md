`mysql -u USERNAME -p` 
Enter in password Prompted 

`hostname -i` 
Within docker exec to find ip address

Link mysql database via hostname within js source file via node mysql connector to setup db.

```
import * as mysql from 'mysql';

// Connections
let connection = mysql.createConnection({
    host: 'mysql', // mysql is name of service container
    user: 'user',
    password: 'password',
    database: 'db',
    port: 3306
});

connection.connect();
```

```
server: 
...
    depends_on:
      - mysql
    links:
      - mysql:mysql
mysql: 
...

```

Setting up app in 172.27.0.3 with hosts set to docker-local.localhost

Volume set to project view Easy Access to server Files.

```
src
  -- *.ts
```

Setting up db in 172.27.0.2 with hosts set to docker-local.localhost

Only works properly within the docker container due to connection with mysql db 

```
npm start 
```

Use `Makefile` to run Development or Production environment

### Development Env

```
make dev
```

### Production Env

```
make prod
```


Change docker-compose file on server to include ip address of droplet 
Change droplet password by siging in with root 

```


    environment:
      STAGE: local
      DOMAINS: '165.22.43.109'
      FORCE_RENEW: 'true'
    volumes:
      - "./dist:/var/www/vhosts/165.22.43.109"
      - "./config/certs:/var/lib/https-portal"

```

### How to 

Once ssh into server. 

```
cd ..
cd home 
git clone https://stevenanthonyrevo@bitbucket.org/stevenanthonyrevo/typescript-docker.git nameofdirectory 
cd nameofdirectory 
vi docker-compose.yml 
```
Edit `environment: DOMAIN` to your real domain and `volumes: "/var/www/vhosts/"` to your real domain

EX:
```
    environment:
      STAGE: production
      DOMAINS: test.sidebuckets.com
      FORCE_RENEW: 'true'
    volumes:
      - "./dist:/var/www/vhosts/test.sidebuckets.com"
```

Then ESC button `:wq!` to overwrite. 

Then `docker-compose up` will start and setup ssl to new domain

alternative: 

```
droplet's ip address instead of domain to test ssl 
```

Running Composer locally 

```
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
```