<?php 

require __DIR__ . '/functions/mysql/getEmployees.php';
require __DIR__ . '/functions/mysql/getEmployee.php';
require __DIR__ . '/functions/mysql/getEmployeeId.php';
require __DIR__ . '/functions/mysql/getHostInfo.php';
require __DIR__ . '/functions/mysql/addEmployee.php';
require __DIR__ . '/functions/mysql/showEmployesTables.php';
require __DIR__ . '/functions/mysql/showDatabases.php';
require __DIR__ . '/functions/mysql/getEmployeesFields.php';

require __DIR__ . '/functions/mailchimp/members.php';
require __DIR__ . '/functions/mailchimp/email.php';
require __DIR__ . '/functions/mailchimp/campaign.php';
require __DIR__ . '/functions/mailchimp/image.php';
require __DIR__ . '/functions/mailchimp/schedule.php';