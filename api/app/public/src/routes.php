<?php
// Routes

$app->get('/', function ($request, $response, $args) {
    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->group('/api', function () use ($app) {
 
    // Version group
    $app->group('/v1', function () use ($app) {
      // Get
      $app->get('/show',                           'showDatabases');
      $app->get('/host',                           'getHostInfo');
      $app->get('/tables',                         'showEmployesTables');
      $app->get('/fields',                         'getEmployeesFields');
      $app->get('/employees',                      'getEmployees');
      $app->get('/employee',                       'getEmployee');
      $app->get('/employee/{id}',                  'getEmployeeId');
      $app->get('/curl',                           'members'); //example of curl and filter 
      $app->get('/template',                       'email');
      $app->get('/upload/{name}',                  'image');
      // Post
      $app->post('/create',                        'addEmployee');
      $app->post('/template/{title}/{banner}',     'campaign');
      $app->post('/test',                          'schedule');
      // Put
      $app->put('/update/{id}',                    'updateEmployee');
      // Delete
      $app->delete('/delete/{id}',                 'deleteEmployee');
	  });
});
