<?php

function members() { 

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://us17.api.mailchimp.com/3.0/lists/e62b670fbe/members",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic dXNlcjo0NzA5MzcwNWNlNWUyYThkYjVkYjA5OTRhOTEwOTc2YS11czE3",
    "cache-control: no-cache",
    "postman-token: 76d03410-bc7d-0335-aaba-e0e85911d0fb"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  $result = json_decode($response);
  $arr = $result->members;

  foreach ($arr as $key => $value) {
    # code...
    
    # Debugging 
    // print_r($value->id);

    # API OPTIONS FOR MAILCHIMP FILTER 
    //value->id 
    //value->ip_signup
    //value->location->latitude
    //value->location->longitude

    $exported[] = $value->email_address;
  }
  
  # Debugging 
  //print_r($exported);
  echo json_encode($exported);
}

}

?>