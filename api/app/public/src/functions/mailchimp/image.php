<?php 

function image(Slim\Http\Request $request, Slim\Http\Response $response, array $args) {

//ex: geeksforgeeks-22
$paramName = $args['name'];

# if filename exists 
if (file_exists(__DIR__ . '/../uploads/' . $paramName . '.png')) {
  # Get the image and convert into string only
  $img = file_get_contents( __DIR__ . '/../uploads/' . $paramName . '.png');
 } else {
   print_r('wrong name for image, not found in uploads directory ');
 }
  
// Encode the image string data into base64 
$data = base64_encode($img); 

$fields = array(
	'name' => $paramName, //name is same as url passed param
  'file' => (string)$data
);

// Debugging
//print_r($fields);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://us17.api.mailchimp.com/3.0/file-manager/files",
  CURLOPT_POST => count($fields),
  CURLOPT_POSTFIELDS => '{"name":"' . $fields['name'] . '.png", "file_data": "' . $fields['file'] .'"}',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/js',
    "Authorization: Basic dXNlcjo0NzA5MzcwNWNlNWUyYThkYjVkYjA5OTRhOTEwOTc2YS11czE3",
    "cache-control: no-cache",
    "postman-token: 76d03410-bc7d-0335-aaba-e0e85911d0fb"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  print_r('err');
  print_r($err);
} else {

 # Debugging
 // print_r($response);

 # Grab new uploaded image URL 
 $result = json_decode($response);
 $imageURL = $result->full_size_url;

# JSON encoded 
 //print_r(json_encode($imageURL));

# NO encoded
 print_r($imageURL);
 
 # Debugging 
 //print_r($exported);

}


}

?>