<?php 

function showEmployesTables() {
  $sql = "SHOW TABLES FROM db";
  try {

  $mysqli = new mysqli("mysql:3306", "user", "password", "db");
    
  $result = $mysqli->query($sql);
  if ($result === false) {
      throw new Exception("Could not execute query: " . $mysqli->error);
  }

  $table_names = array();
  while($row = $result->fetch_array(MYSQLI_NUM)) { // for each row of the resultset
      $table_names[] = $row[0]; //add table name between []
  }
  
  $exported = json_encode($table_names); 
  echo $exported;

   } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

?>