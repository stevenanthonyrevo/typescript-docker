<?php 

function getHostInfo() {
    try {
      $mysqli = new mysqli("mysql:3306", "user", "password", "db");

      if ($mysqli->connect_errno) {
          echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
      }
      
      return json_encode($mysqli);

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

?>