<?php 

function getAll() 
{
  $sql = "SELECT * FROM MyGuests";
  try {

  $mysqli = new mysqli("mysql:3306", "user", "password", "db");
    
  $result = $mysqli->query($sql);

  if ($result === false) {
      throw new Exception("Could not execute query: " . $mysqli->error);
  }

  $columns = array();
  $resultset = array();

  //get fieldnames 
  while ($row = mysqli_fetch_assoc($result)) {
      if (empty($columns)) {
          $columns = array_keys($row); //have field array of rows
          $results = $columns; //map to array values of row
      }
  }

  $exported = json_encode($results); 
  echo $exported;

  } catch(PDOException $e) {
      echo '{"error":{"text":'. $e->getMessage() .'}}';
  }
}


?>