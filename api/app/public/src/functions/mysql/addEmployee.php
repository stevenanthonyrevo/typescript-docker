<?php 
function addEmployee() {
  // sql query with CREATE TABLE
  $sql = "CREATE TABLE IF NOT EXISTS MyGuests (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, firstname VARCHAR(30) NOT NULL, lastname VARCHAR(30) NOT NULL, email VARCHAR(50), reg_date TIMESTAMP)";
    try {

      $mysqli = new mysqli("mysql:3306", "user", "password", "db");

      if ($mysqli->connect_errno) {
          echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
      }

      // Performs the $sql query on the server to create the table
      if ($mysqli->query($sql) === TRUE) {
         return json_encode('Table "MyGuests" successfully created');
      }
      else {
        echo 'Error: '. $mysqli->error;
      }

      $mysqli->close();
  } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

?>