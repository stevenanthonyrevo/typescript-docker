<?php 

function showDatabases() {
  $sql = "SHOW DATABASES";
  try {

  $mysqli = new mysqli("mysql:3306", "user", "password", "db");
    
  $result = $mysqli->query($sql);
  if ($result === false) {
      throw new Exception("Could not execute query: " . $mysqli->error);
  }

  $db_names = array();
  while($row = $result->fetch_array(MYSQLI_NUM)) { // for each row of the resultset
      $db_names[] = $row[0]; // Add db name to $db_names array
  }

  $exported = json_encode($db_names); 
  echo $exported;

   } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

?>