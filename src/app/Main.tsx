import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Home from "./routes/Home";
import Stuff from "./routes/Stuff";
import Contact from "./routes/Contact";
import Counter from './routes/Counter';
import Users from './routes/Users';
import Emails from './routes/Emails';
import Images from './routes/Images';

class Main extends Component {
  render() {
    return (
        <HashRouter>
        <div>
          <h1>Simple SPA</h1>
          <ul className="header">
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/stuff">Stuff</NavLink></li>
            <li><NavLink to="/contact">Contact</NavLink></li>
            <li><NavLink to="/counter">Counter</NavLink></li>
            <li><NavLink to="/users">Users</NavLink></li>
            <li><NavLink to="/emails">Emails</NavLink></li>
            <li><NavLink to="/images">Images</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={Home}/>
            <Route path="/stuff" component={Stuff}/>
            <Route path="/contact" component={Contact}/>
            <Route path="/counter" component={Counter}/>
            <Route path="/users" component={Users}/>
            <Route path="/emails" component={Emails}/>
            <Route path="/images" component={Images}/>
          </div>
        </div>
      </HashRouter>
    );
  }
}
 
export default Main;