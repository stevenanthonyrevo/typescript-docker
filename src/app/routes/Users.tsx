import * as React from 'react';

type MyState = { 
  isLoading: any,
  users: any,
  error: any,
};

class Users extends React.Component<any, MyState> {
  state = {
    isLoading: true,
    users: [] as any[],
    error: [] as any[],
  };

  fetchUsers() {
    fetch(`http://localhost:3300/api/v1/employee`)
      .then(response => response.json())
      .then(data =>
        this.setState({
          users: data,
          isLoading: false,
        })
      )
      .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.fetchUsers();
  }
  render() {
    const { isLoading, users, error } = this.state;
    return (
      <React.Fragment>
        <h1>Random User</h1>
        {error ? <p>{error}</p> : null}
        {!isLoading ? (
          users.map(user => {
            const { id, firstname, lastname, email } = user;
            return (
              <div key={id}>
                <p>First Name: {firstname}</p>
                <p>Last Name: {lastname}</p>
                <p>Email Address: {email}</p>
                <hr />
              </div>
            );
          })
        ) : (
          <h3>Loading...</h3>
        )}
      </React.Fragment>
    );
  }
}
export default Users;