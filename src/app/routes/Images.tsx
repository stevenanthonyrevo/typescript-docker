import * as React from 'react';

type MyState = { 
  url: any,
  isLoading: any,
  images: any,
  error: any,
  title: string,
  banner: string
};

let title = `New Test world A Template`;
let banner = `https://gallery.mailchimp.com/4d16cd479d20903aefbf9f229/images/3da2f5f3-a121-4f00-bf12-a18a7ccce831.png`;

let url = `http://localhost:3300/api/v1/template/test/&?title=${title}&banner=${banner}`;

class Images extends React.Component<any, MyState> {
  state = {
    url: '' as string,
    isLoading: true,
    images: [] as any[],
    error: [] as any[],
    title: '' as string,
    banner: '' as string,
  };

// http://localhost:3300/api/v1/template/test/&?title=New Test world A Template&banner=https://gallery.mailchimp.com/4d16cd479d20903aefbf9f229/images/3da2f5f3-a121-4f00-bf12-a18a7ccce831.png
  fetchImage() {
    fetch(url, {
      method: 'POST',
      headers: new Headers({
          'Content-Type': 'application/json', // <-- Specifying the Content-Type
        })
    })
    .then((response) => response.text())
    .then((data) => {
      console.log(data);
      this.setState({
        images: JSON.stringify(data),
        isLoading: false,
      })
    })
    .catch((error) => {
        console.error(error);
    });
  }

  componentDidMount() {
    //this.fetchImage();
  }
  
  render() {
    const { isLoading, images, error } = this.state;
    return (
      <React.Fragment>
        <h1>Mailchimp Emails</h1>
        <button onClick={this.fetchImage.bind(this)}>Create</button>
        {error ? <p>{error}</p> : null}
        {!isLoading ? (
              <div>
                <p>Image URl: {images}</p>
                <hr />
              </div>
        ) : (
          <h3>Loading...</h3>
        )}
      </React.Fragment>
    );
  }
}
export default Images;

