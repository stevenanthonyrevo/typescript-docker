import * as React from 'react';

type MyState = { 
  isLoading: any,
  emails: any,
  error: any,
};

class Emails extends React.Component<any, MyState> {
  _isMounted = false;

  state = {
    isLoading: true,
    emails: [] as any[],
    error: [] as any[],
  };

  fetchUsers() {
    fetch(`http://localhost:3300/api/v1/curl`)
      .then(response => response.json())
      .then(data =>
        this.setState({
          emails: data,
          isLoading: false,
        })
      )
      .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this._isMounted = true;
    this.fetchUsers();
  }
  render() {
    const { isLoading, emails, error } = this.state;
    return (
      <React.Fragment>
        <h1>Mailchimp Emails</h1>
        {error ? <p>{error}</p> : null}
        {!isLoading ? (
          emails.map(email => {
            return (
              <div key={email}>
                <p>Email: {email}</p>
                <hr />
              </div>
            );
          })
        ) : (
          <h3>Loading...</h3>
        )}
      </React.Fragment>
    );
  }
}
export default Emails;